# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class stock_inventory(models.Model):

    _inherit = 'stock.inventory'

    _logger = logging.getLogger(__name__)

    comment = fields.Text(
        string='Comment',
        required=False,
        readonly=False,
        help='Justify the adjustments of inventory',
        translate=True
    )